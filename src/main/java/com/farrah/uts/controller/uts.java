package com.farrah.uts.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/farrah")
public class uts {

    @RequestMapping(value = {"/uts", ""}, method = RequestMethod.GET)
    public String uts() {
        return "view/uts";
    }
}
